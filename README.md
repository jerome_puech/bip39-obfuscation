# BIP39 obfuscation

With this tool, you can generate a list of random 24 words (mostly used as cryptocurrencies recovery phrases), and then you can hide your true recovery phrase inside all those fake ones.

## Usage :

```bash
chmod +x BIP39_obfuscation.bash
./BIP39_obfuscation.bash
```

## Example :

```
-----------
Numéro 1
-----------
core
meat
symbol
cool
ice
leaf
pill
next
car
team
frost
canvas
humor
worry
eyebrow
split
rival
author
silly
harbor
raise
select
radio
soft
-----------
Numéro 2
-----------
dutch
alter
knock
field
dad
gym
insane
kick
source
globe
hurdle
frog
action
tumble
twenty
holiday
detect
hand
solid
element
fit
grief
gas
churn
-----------
Numéro 3
-----------
assault
october
sunset
melody
genre
dumb
wire
job
legal
cry
shed
coffee
height
focus
lyrics
enter
very
holiday
total
pizza
evil
purity
harvest
finger

```


## License :
[GPLv3](https://www.gnu.org/licenses/gpl-3.0-standalone.html)
